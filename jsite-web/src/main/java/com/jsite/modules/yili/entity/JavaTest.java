/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.entity;

import org.hibernate.validator.constraints.Length;
import com.jsite.modules.sys.entity.Office;

import com.jsite.common.persistence.DataEntity;

/**
 * JAVA机试表生成Entity
 * @author liuruijun
 * @version 2020-06-09
 */
public class JavaTest extends DataEntity<JavaTest> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 人员编号
	private String name;		// 姓名
	private String sex;		// 性别
	private Integer age;		// 年龄
	private String mobile;		// 手机号
	private Office office;		// 部门id
	
	public JavaTest() {
		super();
	}

	public JavaTest(String id){
		super(id);
	}

	@Length(min=0, max=255, message="人员编号长度必须介于 0 和 255 之间")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=255, message="姓名长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=1, message="性别长度必须介于 0 和 1 之间")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	@Length(min=0, max=32, message="手机号长度必须介于 0 和 11 之间")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}
	
}